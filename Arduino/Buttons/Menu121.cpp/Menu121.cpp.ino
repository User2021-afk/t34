// -=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=- //
// Меню для Ardyino_dev board
// V 1.0
// -=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=- //

//-------------------------------------------------БИБЛИОТЕКИ---------------------------------------//

#include <Wire.h>
#include "Button_Simple.h"
#include <Arduino.h>
#include <OneWire.h>
#include <DallasTemperature.h>
#include <NewPing.h>
#include <LedControl.h>
#include "LiquidCrystal_I2C_Menu_Btns.h"
LiquidCrystal_I2C_Menu_Btns lcd(0x27, 16, 2);

//-------------------------------------------------------------------НАСТРОЙКА ВСЕХ УСТРОЙСТВ-------------------------//

const int LIMIT_UP = 4;
const int LIMIT_DOWN = 0;

const int MOTOR_PWM = 3;
void motor(int pwm);


Button_Simple button13(13); // Пин кнопки перелистывания вверз
Button_Simple button16(16); // Пин кнопки входа в меню
Button_Simple button17(17); // Пnnnин кнопки перелистывания вниз

const int ONE_WIRE_BUS = 2; // pin для датчика t
OneWire oneWire(ONE_WIRE_BUS);
DallasTemperature sensors(&oneWire);
LedControl lc = LedControl(5, 6, 7, 1); // обект мтрицы

// Пины для подключения Сонара
const int PIN_ECHO = 14;
const int PIN_TRIG = 15;
NewPing sonar(PIN_TRIG, PIN_ECHO, 400);

#define pinLeft  16
#define pinRight 13
#define pinEnter 17

//здесь обявляются служебные ключи, они пригодятся в будущем
enum {mkLED_M1, mkLED_M2, mkMotor_Y, mkMotor_N, mkTemp, mkStatic, mkLED_G, mkMatrix, mkMotor, mkSonar, mkBack, mkRoot};

// Описаие меню
// структура пункта меню: {ParentKey, Key, Caption, [Handler]}
sMenuItem menu[] = {
  {mkBack, mkRoot, "Menu dev_board V2.0"},
  {mkRoot, mkMatrix, "Matrix_LED"},
  {mkRoot, mkLED_M1, "Smail ><"},
  {mkRoot, mkLED_M2, "Smail x"},
  {mkRoot, mkMotor, "Motor"},
  {mkMotor, mkMotor_Y, "motor yes"},
  {mkMotor, mkMotor_N, "motor no"},
  {mkRoot, mkStatic, "Analog"},
  {mkRoot, mkLED_G, "LED_GIFX"},
  {mkRoot, mkTemp, "Temperatur"},
  {mkRoot, mkSonar, "Sonar"},

};

uint8_t menuLen = sizeof(menu) / sizeof(sMenuItem);
unsigned long tim; // таймер на millis

void setup() {
  lcd.begin();
  lcd.attachButtons(pinLeft, pinRight, pinEnter);
  sensors.begin();// запускаем датчик 

  // Светодиодная матрица 8х8
  lc.shutdown(0, false);
  lc.setIntensity(0, 1);
  lc.clearDisplay(0);

  pinMode(PIN_TRIG, OUTPUT);
  pinMode(3, OUTPUT);// мотор
  pinMode(8, OUTPUT);
  pinMode(9, OUTPUT);
  pinMode(10, OUTPUT);
  pinMode(11, OUTPUT);
  pinMode(12, OUTPUT);
}

void loop() {
  static int click13 = 0; // Состояние режима кнопки 1/0
  static int click16 = 0; // Состояние режима кнопки 1/0
  static int click17 = 0; // Состояние режима кнопки 1/0
  button13.scan();        // Инициализация библиотеки
  button16.scan();        // Инициализация библиотеки
  button17.scan();        // Инициализация библиотеки
  static int COUNT = 0;   // Счётчик
  // Инкремент счётчика
  while (button17.clicked()) { // Если нажали
    if (++click17 >= 2) { click17 = 0; ++COUNT; } // Счётчик с ограничителем (0/1)
    if (click17 == 1) { ++COUNT; }
  }
  // Декремент счётчика
  while (button13.clicked()) {
    if (++click13 >= 2) { click13 = 0; --COUNT; } // Счётчик с ограничителем (0/1)
    if (click13 == 1) { --COUNT; }
  }
  while (button16.clicked()) {
    if (++click16 >= 2) { click16 = 0; } // Счётчик с ограничителем (0/1)
  }
  while (COUNT >= LIMIT_UP) { COUNT = LIMIT_UP; break; } // Ограничение инкремента
  while (COUNT <= LIMIT_DOWN) { COUNT = LIMIT_DOWN; break;  } // Ограничение декремента
  switch (COUNT)
  {
  // Покозывем меню
  uint8_t selectedMenuItem = lcd.showMenu(menu, menuLen, 1);
  // И выполняем действия в соответствии с выбранным пунктом
  case 0:
    lcd.setCursor(0, 0);      // курсор на 4-й символ 1-й строки
    lcd.print("  Choose menu   "); // Тест на 1-й строке экрана
    lcd.setCursor(0, 1);  // курсор/строка
    lcd.print("Menu Temperature:"); // Тест на 2-й строке экрана
    if (click16 == 1) {
  lcd.clear(); // Очищаем экран перед получением нового значения
  lcd.setCursor(0, 0); // курсор на 4-й символ 1-й строки
  lcd.print("Temperature:"); // Тест на 1-й строке экрана
  lcd.setCursor(2, 1); // курсор на 7-й символ 2-й строки
  lcd.print(sensors.getTempCByIndex(0)); // Значение t на 2-й строке экрана 
    }
    else { break; }
    break;
  case 1:
    lcd.setCursor(2, 0);      // курсор на 4-й символ 1-й строки
    lcd.print("  Choose menu   "); // Тест на 1-й строке экрана
    lcd.setCursor(0, 1);
    lcd.print("Menu Motor:     ");
    if (click16 == 1) {
      lcd.setCursor(0, 0);      // курсор/строка
      lcd.print("    Motor pwm   "); // Тест на 1-й строке экрана
      lcd.setCursor(0, 1);
      lcd.print("     start      ");
      motor(255);
      delay(1000);
    }
    else { break; }
    motor(0);
    break;
  case 2:
    lcd.setCursor(2, 0);      // курсор на 4-й символ 1-й строки
    lcd.print("  Choose menu   "); // Тест на 1-й строке экрана
    lcd.setCursor(0, 1);
    lcd.print("Menu Voice:     ");
    if (click16 == 1) {
      lcd.setCursor(0, 0);      // курсор/строка
      lcd.print("   Voice beep   "); // Тест на 1-й строке экрана
      lcd.setCursor(0, 1);
      lcd.print("      beep      ");
      delay(100);
      voice(155);
    }
    else { break; }
    voice(0);
    break;
  case 3:
    lcd.setCursor(2, 0);      // курсор на 4-й символ 1-й строки
    lcd.print("  Choose menu   "); // Тест на 1-й строке экрана
    lcd.setCursor(0, 1);
    lcd.print("8x8 led matrix:");
    if (click16 == 1) {
      lcd.setCursor(0, 0);      // курсор/строка
      lcd.print("      Matrix    "); // Тест на 1-й строке экрана
      lcd.setCursor(0, 1);
      lcd.print("     Start      ");
      delay(100);
      ledMatrix();
    }
    else { break; }
    break;
  case 4:
    lcd.setCursor(2, 0);      // курсор на 4-й символ 1-й строки
    lcd.print("  Choose menu   "); // Тест на 1-й строке экрана
    lcd.setCursor(0, 1);
    lcd.print("Menu Sonar:     ");
    if (click16 == 1) {
      lcd.setCursor(0, 0); // курсор/строка
      lcd.print("      Sonar     "); // Тест на экране дисплея
      lcd.setCursor(0, 1); // курсор/строка
      lcd.print("distance: "); // Тест на экране дисплея
      lcd.setCursor(12, 1);
      lcd.print(sonar.ping_cm());
      lcd.setCursor(15, 1); // курсор/строка
      lcd.print(" "); // Тест на экране дисплея
      delay(100);
    }
    else { break; }
    break;
  default:
    lcd.setCursor(2, 0);      // курсор на 4-й символ 1-й строки
    lcd.print("  Choose menu   "); // Тест на 1-й строке экрана
    lcd.setCursor(0, 1);
    lcd.print("Menu 1:         ");
    break;
  }
  //Serial.println(COUNT); // Для дианостики
}

void ledMatrix() {
  // Проверка 8х8 светодиодной матрицы
  //lc.setLed(0, 3, 4, true);
  lc.setRow(0, 0, B11111111);
  lc.setRow(0, 1, B11111111);
  lc.setRow(0, 2, B11100111);
  lc.setRow(0, 3, B11011011);
  lc.setRow(0, 4, B11100111);
  lc.setRow(0, 5, B11111111);
  lc.setRow(0, 6, B11111111);
  lc.setRow(0, 7, B11111111);
}

void voice(int on) {
  // Проверка динамика
  analogWrite(4, on);
  delay(150);
  analogWrite(4, 0);
  delay(150);
}

void motor(int pwm) {
  // Проверка мотора
  analogWrite (MOTOR_PWM, pwm);
}
