// -=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=- //
// China car. Biathlon, KegelRin
// V 1.0
// -=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=- //
#include <Arduino.h>
//#include <NewPing.h>
// Пины моторов
const int ENA = 3;
const int IN1 = 5;
const int IN2 = 6;
const int IN3 = 7;
const int IN4 = 8;
const int ENB = 9;
// Пины трэк-сенсоров
const int SENS_LEFT   = 14;
const int SENS_CENTER = 15;
const int SENS_RIGHT  = 16;
const int SENS_LEFTER = 17;
const int SENS_RIGHTER = 18;
// Пины сонара
//const int PIN_ECHO = 17;
//const int PIN_TRIG = 18;

//NewPing sonar(PIN_TRIG,PIN_ECHO);
// Скорость движения по прямой
const int SPEED_LEFT = 80;
const int SPEED_RIGHT = 90;
//-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-
const int SPEED_TURN_LEFT = 95; // Скорость на поворотах влево
const int SPEED_TURN_RIGHT = 95; // Скорость на поворотах вправо
//-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-
//Объявление функций
void moveGo(int speed_left, int speed_right, int times);
void moveBack(int speed_left, int speed_right, int times);
void moveStop(int times);
void turnLeft(int speed_turn_left, int speed_turn_right, int times);
void turnRight(int speed_turn_left, int speed_turn_right, int times);
void sensTest(int times);

void setup() {
  Serial.begin(9600);
  pinMode(ENA, OUTPUT);
  pinMode(IN1, OUTPUT);
  pinMode(IN2, OUTPUT);
  pinMode(IN3, OUTPUT);
  pinMode(IN4, OUTPUT);
  pinMode(ENB, OUTPUT);
  //pinMode(PIN_TRIG, OUTPUT);
}

void loop() {
  //sensTest(100); // Для диагностики датчиков отражения
  //Serial.println(sonar.ping_cm());
  //moveGo(SPEED_LEFT, SPEED_RIGHT, 2000);
  //moveBack(SPEED_LEFT, SPEED_RIGHT, 2000);
  
  bool sLeft = !digitalRead(SENS_LEFT), sCenter = !digitalRead(SENS_CENTER), sRight = !digitalRead(SENS_RIGHT);
  if ((sLeft == false) && (sCenter == true) && (sRight == false)) { // 010
    Serial.println("Go!");
    moveGo(SPEED_LEFT, SPEED_RIGHT, 80);
  }
  else if ((sLeft == true) && (sCenter == false) && (sRight == false)) { // 100
    Serial.println("LEFT!");
    turnLeft(SPEED_TURN_LEFT, SPEED_TURN_RIGHT, 17);
  }
  else if ((sLeft == false) && (sCenter == false) && (sRight == true)) { // 001
    Serial.println("Right!");
    turnRight(SPEED_TURN_LEFT, SPEED_TURN_RIGHT, 17);
  }
  else if ((sLeft == true) && (sCenter == true) && (sRight == false)) { // 110
    Serial.println("LEFT!");
    turnLeft(SPEED_TURN_LEFT, SPEED_TURN_RIGHT, 17);
  }
  else if ((sLeft == false) && (sCenter == true) && (sRight == true)) { // 011
    Serial.println("Right!");
    turnRight(SPEED_TURN_LEFT, SPEED_TURN_RIGHT, 17);
  }
  else if ((sLeft == true) && (sCenter == false) && (sRight == true)) { // 101
    Serial.println("Go!");
    moveGo(SPEED_LEFT, SPEED_RIGHT, 1);
  }
  else if ((sLeft == false) && (sCenter == false) && (sRight == false)) { // 000
    moveStop(300);
    moveGo(255, 255, 15);
  }
  else { // 111
    Serial.println("Go!");
    moveGo(SPEED_LEFT, SPEED_RIGHT, 1);
  }
  
}

void moveGo(int speed_left, int speed_right, int times) {
  analogWrite(ENB, speed_left);
  digitalWrite(IN1, LOW);
  digitalWrite(IN2, HIGH);
  analogWrite(ENA, speed_right);
  digitalWrite(IN3, LOW);
  digitalWrite(IN4, HIGH);
  delay(times);
}

void turnBack(int speed_left, int speed_right, int times) {
  analogWrite(ENB, speed_left);
  digitalWrite(IN1, HIGH);
  digitalWrite(IN2, LOW);
  analogWrite(ENA, speed_right);
  digitalWrite(IN3, HIGH);
  digitalWrite(IN4, LOW);
  delay(times);
}

void moveStop(int times) {
  analogWrite(ENA, 0);
  analogWrite(ENB, 0);
  delay(times);
}

void turnLeft(int speed_turn_left, int speed_turn_right, int times) {
  analogWrite(ENB, speed_turn_left);
  digitalWrite(IN1, LOW);
  digitalWrite(IN2, HIGH);
  analogWrite(ENA, speed_turn_right);
  digitalWrite(IN3, HIGH);
  digitalWrite(IN4, LOW);
  delay(times);
}

void turnRight(int speed_turn_left, int speed_turn_right, int times) {
  analogWrite(ENB, speed_turn_left) ;
  digitalWrite(IN1, HIGH);
  digitalWrite(IN2, LOW);
  analogWrite(ENA, speed_turn_right);
  digitalWrite(IN3, LOW);
  digitalWrite(IN4, HIGH);
  delay(times);
}

void sensTest(int times) {
  bool sLeft = 0; bool sCenter = 0; bool sRight = 0;
  Serial.print("Left: ");
  Serial.print(sLeft = !digitalRead(SENS_LEFT));
  Serial.print(" ");
  Serial.print("Center: ");
  Serial.print(sCenter = !digitalRead(SENS_CENTER));
  Serial.print(" ");
  Serial.print("Right: ");
  Serial.println(sRight = !digitalRead(SENS_RIGHT));
  delay(times);
}
/*
    turnGo(SPEED_LEFT, SPEED_RIGHT, 2000);
    turnStop(2000);
    turnBack(SPEED_LEFT, SPEED_RIGHT, 2000);
    turnStop(2000);
    turnLeft(SPEED_LEFT, SPEED_RIGHT, 2000);
    turnStop(2000);
    turnRight(SPEED_LEFT, SPEED_RIGHT, 2000);
    turnStop(2000);
*/
// -=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=- //
// END FILE
// -=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=- //
