#include <NewPing.h>
const unsigned int ECHO_PIN = 11;
const unsigned int TRIG_PIN = 12;

NewPing sonar(TRIG_PIN, ECHO_PIN, 200); // Максимальное расстояние видимости

const int ENA = 9;
const int IN1 = 7;
const int IN2 = 6;
const int IN3 = 5;
const int IN4 = 4;
const int ENB = 3;

void setup() {
  pinMode(ENA, OUTPUT); 
  pinMode(IN1, OUTPUT); 
  pinMode(IN2, OUTPUT); 
  pinMode(IN3, OUTPUT); 
  pinMode(IN4, OUTPUT);
  pinMode(ENB, OUTPUT);  
}

void loop() {
  delay(2000);
  //вперед
  analogWrite(ENA, 255); // Левый мотор
  digitalWrite(IN1, HIGH);
  digitalWrite(IN2, LOW);
  
  digitalWrite(IN3, HIGH); 
  digitalWrite(IN4, LOW);
  analogWrite(ENB, 255);//ENB ПРАВЫЙ МОТОР
  
  delay(2000);
  //остановка
  analogWrite(ENA, 0); 
  digitalWrite(IN1, LOW);
  digitalWrite(IN2, HIGH);
  
  digitalWrite(IN3, LOW); 
  digitalWrite(IN4, HIGH);
  analogWrite(ENB, 0);

  delay(2000);
  //назад
  analogWrite(ENA, 255); 
  digitalWrite(IN1, LOW);
  digitalWrite(IN2, HIGH);
  
  digitalWrite(IN3, LOW); 
  digitalWrite(IN4, HIGH);
  analogWrite(ENB, 255);
   
  delay(2000);
  //остановка
  analogWrite(ENA, 0); 
  digitalWrite(IN1, LOW);
  digitalWrite(IN2, HIGH);
  
  digitalWrite(IN3, LOW);
  digitalWrite(IN4, HIGH);
  analogWrite(ENB, 0);
  
  delay(2000);
  //вправо
  analogWrite(ENA, 225); 
  digitalWrite(IN1, HIGH);
  digitalWrite(IN2, LOW);
  
  digitalWrite(IN3, HIGH); 
  digitalWrite(IN4, LOW);
  analogWrite(ENB, 0);
  
  delay(2000);
  //остановка
  analogWrite(ENA, 0); 
  digitalWrite(IN1, LOW);
  digitalWrite(IN2, HIGH);
  
  digitalWrite(IN3, LOW); 
  digitalWrite(IN4, HIGH);
  analogWrite(ENB, 0);
  
  delay(2000);
  //влево
  analogWrite(ENA, 0); 
  digitalWrite(IN1, HIGH);
  digitalWrite(IN2, LOW);
  
  digitalWrite(IN3, HIGH);
  digitalWrite(IN4, LOW);
  analogWrite(ENB, 225);

  delay(2000);
  //остановка
  analogWrite(ENA, 0); 
  digitalWrite(IN1, LOW);
  digitalWrite(IN2, HIGH);
  
  digitalWrite(IN3, LOW); 
  digitalWrite(IN4, HIGH);
  analogWrite(ENB, 0);
  
  delay(2000);
  
}
