#include <Arduino.h>
#include <NewPing.h>
#include <Wire.h>
const int PIN_TRIG = 13;
const int PIN_ECHO = 12;

NewPing sonar(PIN_TRIG, PIN_ECHO, 200); // Максимальное расстояние видимости

const int ENA = 9;
const int IN1 = 7;
const int IN2 = 6;
const int IN3 = 5;
const int IN4 = 4;
const int ENB = 3;

void setup() {
  Serial.begin(9600);
  pinMode(ENA, OUTPUT);
  pinMode(IN1, OUTPUT);
  pinMode(IN2, OUTPUT);
  pinMode(IN3, OUTPUT);
  pinMode(IN4, OUTPUT);
  pinMode(ENB, OUTPUT);
  pinMode(PIN_TRIG, OUTPUT);
  pinMode(PIN_ECHO, INPUT);
}

void loop() {
  /*
    // Правая сторона вперёд
    analogWrite(ENA, 255);
    digitalWrite(IN1, HIGH);
    digitalWrite(IN2, LOW);
    // Левая сторона вперёд
    digitalWrite(IN3, HIGH);
    digitalWrite(IN4, LOW);
    analogWrite(ENB, 255);
  */
  int distance = sonar.ping_cm();
  Serial.println(distance);
  if ((distance > 1) && (distance < 30)) {
    //вперед
    analogWrite(ENA, 0); // Левый мотор
    analogWrite(ENB, 0);//ENB ПРАВЫЙ МОТОP
    delay(1000);
     //назад
    analogWrite(ENA, 255);
    digitalWrite(IN1, LOW);
    digitalWrite(IN2, HIGH);
    digitalWrite(IN3, LOW);
    digitalWrite(IN4, HIGH);
    analogWrite(ENB, 255);
    delay(750);
    //вправо
    analogWrite(ENA, 225);
    digitalWrite(IN1, HIGH);
    digitalWrite(IN2, LOW);
    digitalWrite(IN3, HIGH);
    digitalWrite(IN4, LOW);
    analogWrite(ENB, 0);
    delay(1000);
  }
  else {
    // Правая сторона вперёд
    analogWrite(ENA, 255);
    digitalWrite(IN1, HIGH);
    digitalWrite(IN2, LOW);
    // Левая сторона вперёд
    digitalWrite(IN3, HIGH);
    digitalWrite(IN4, LOW);
    analogWrite(ENB, 255);
  }
}
