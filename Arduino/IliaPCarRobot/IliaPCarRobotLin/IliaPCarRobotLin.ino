// -=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=- //                 
// Робот с использованием L298P драйвера для биатлона
// V 2.0
// -=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=- //
#include <Arduino.h>
const int ENA = 9;
const int IN1 = 7;
const int IN2 = 6;
const int ENB = 3;
const int IN3 = 5;
const int IN4 = 4;
 
const int SensorLeft = 17; // SENS_LEFT
const int SensorMiddle = 18;
const int SensorRight = 19;

const int SPEED_LEFT_FORWARD = 90;    // Скорость левой стороны вперёд
const int SPEED_RIGHT_FORWARD = 70; // Скорость правой стороны вперёд

const int SPEED_LEFT_BACK = 125;    // Скорость левой стороны назад
const int SPEED_RIGHT_BACK = 145; // Скорость правой стороны назад

const int SPEED_LEFT_TURN = 159;
const int SPEED_RIGHT_TURN = 95;

void setup() {
  Serial.begin(9600);
  pinMode(IN1, OUTPUT);
  pinMode(IN2, OUTPUT);
  pinMode(IN3, OUTPUT);
  pinMode(IN4, OUTPUT);
  pinMode(ENA, OUTPUT); 
  pinMode(ENB, OUTPUT);
}

void loop() {         
  bool SL = digitalRead(SensorLeft), SM = digitalRead(SensorMiddle), SR = digitalRead(SensorRight);   
  if (SL == true && SM == false && SR == true) { // 101
    forward();
    Serial.println("Forward");
  }
  else if (SL == false && SM == true && SR == true) { // 011
    TurnLeft();
    Serial.println(" TurnLeft");  
  }
  else if (SL == true && SM == true && SR == false) { // 110
    TurnRight();
    Serial.println("TurnRight");
  }
  //else if (SL == true && SM == true && SR == false) {
    //TurnRight();
    //Serial.println("TurnRight");
  //}
  else {  
     (SL == true && SM == true && SR == true); // 
    forward();
  }
    
  //forward();
  //delay(1000);
  //STOP();
  //Back();
  //TurnRight();
  //TurnLeft();
  //_testIR(); // Тест датчиков отражения
}

void _testIR() { // Тест датчиков отражения
  bool SL = digitalRead(SensorLeft), SM = digitalRead(SensorMiddle), SR = digitalRead(SensorRight);
  Serial.print("Left: "); Serial.print(SL); Serial.print(" Middle: "); Serial.print(SM);
  Serial.print(" Right: "); Serial.println(SR);
  delay(40);
}

void forward() {
  analogWrite(ENA, SPEED_LEFT_FORWARD); // Левый мотор
  digitalWrite(IN1, HIGH);
  digitalWrite(IN2, LOW);
  digitalWrite(IN3, HIGH);
  digitalWrite(IN4, LOW);
  analogWrite(ENB, SPEED_RIGHT_FORWARD);//ENB ПРАВЫЙ МОТОP
}

void STOP() {
  analogWrite(ENA, 0);
  analogWrite(ENB, 0);
  delay(250);
}

void Back() {
  analogWrite(ENA, SPEED_LEFT_BACK);
  digitalWrite(IN1, LOW);
  digitalWrite(IN2, HIGH);
  digitalWrite(IN3, LOW);
  digitalWrite(IN4, HIGH);
  analogWrite(ENB, SPEED_RIGHT_BACK);
  }

void TurnRight() {
  analogWrite(ENA, SPEED_LEFT_TURN);
  digitalWrite(IN1, LOW);
  digitalWrite(IN2, HIGH);
  digitalWrite(IN3, HIGH);
  digitalWrite(IN4, LOW);
  analogWrite(ENB, SPEED_RIGHT_TURN);
}

void TurnLeft() {
  analogWrite(ENA, SPEED_LEFT_TURN);
  digitalWrite(IN1, HIGH);
  digitalWrite(IN2, LOW);
  digitalWrite(IN3, LOW);
  digitalWrite(IN4, HIGH);
  analogWrite(ENB, SPEED_RIGHT_TURN);
}
// -=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=- //
// END FILE
// -=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=- //
