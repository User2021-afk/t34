//-=-=-=-=-=-=--=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=//
// Программа управления сенсерными кнопками
// V1.0
//-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=//
//#include "RichUNOTM1637.h"
#include <LiquidCrystal_I2C.h> // Библиотека I2C дисплея
//#include "LiquidCrystal_I2C_Menu_Btns.h"
LiquidCrystal_I2C lcd(0x27, 16, 2);
const int CLK = 10;
const int DIO = 11;
//TM1637 disp(CLK, DIO);

const int pinRead = 13;
const int pinLeft = 16;
const int pinEhat = 17;

void setup() {
  pinMode(8, OUTPUT);
  pinMode(9, OUTPUT);
  pinMode(10, OUTPUT);
  pinMode(11, OUTPUT);
  pinMode(12, OUTPUT);

  Serial.begin(9600);
  lcd.begin(16, 2);
  lcd.backlight();
    lcd.init(); // Инициализация LCD
}

void loop() {
  if (digitalRead(pinRead) == true) {
    lcd.clear();
   lcd.setCursor(0, 0);      // курсор на 4-й символ 1-й строки
    lcd.print(" Hello World! "); // Тест на 1-й строке экрана
  }

  if (digitalRead(pinLeft) == true) {
    lcd.clear();
    lcd.setCursor(0, 0);
     lcd.print(" Hi! ");
  }
}
